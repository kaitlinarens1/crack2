#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.

int file_length(char *filename)
{
    struct stat info;
    int ret = stat(filename, &info);
    if(ret == -1)
    {
        return -1;
    }
    else
    {
        return info.st_size;
    }

}


int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    
    //char hashguess[HASH_LEN];
    char *guessmd5 = md5(guess, strlen(guess));
    
    //printf("md5: %s\n", md5(guess,strlen(guess)));
    //sprintf(hashguess,"%s", md5(guess,strlen(guess)));
    
    /*if(hash[strlen(hash)-1]== '\n')
    {
        hash[strlen(hash)-1] = '\0';
    }*/
    
    if(strcmp(guessmd5, hash) == 0)
    {
        free(guessmd5);
        return 1;
    }
    
    else
    {
        free(guessmd5);
        return 0;
    }
    
    /*for (int i = 0; i < HASH_LEN; i++)
    {
        if(hashguess[i] == hash[i])
        {
            
            return 1;
            //i++;
        }
            
        else 
        {
            
            return 0;
        }
        //free(hash[i]);
        //free(hashguess[i]);
    }*/

   //return 0;
}

char **read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    char *passwd = malloc(len);

    FILE *f = fopen(filename,"r");
    if(!f)
    {
        printf(" Can't open %s for reading.\n", filename);
        exit(1);
    }

    fread(passwd, sizeof(char), len, f);
    fclose(f);
    
    int num_passwds = 0;

    for(int i = 0; i < len; i++)
    {
        if(passwd[i] == '\n')
        {
            passwd[i] = '\0';
            num_passwds++;
        }
    }
    
    // allocate space for array pointers
    char **passwds = malloc(num_passwds * sizeof(char *));

    //load address to array
    passwds[0] = &passwd[0];
    int j = 1;
    for(int i = 0; i < len-1; i++)
    {
        if (passwd[i] == '\0')
        {
            passwds[j]= &passwd[i+1];
            j++;
        }
    }
    *size = num_passwds;
    return passwds;
    fclose(f);
    //free(passwds);
    //free(passwd);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }


    // Read the dictionary file into an array of strings.
    int dlen;
    char **dict = read_dictionary("rockyou100.txt", &dlen);

    // Open the hash file for reading.
    FILE *fp = fopen("hashes.txt","r");
    if(!fp)
    {
        printf("cannot open hashes.txt file\n");
        exit(1);
    }
        
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int count = 0;
    char line[HASH_LEN];
    while(fgets(line, HASH_LEN, fp) != NULL)
    {
        for (int i = 0; i < dlen; i++)
        {
            //printf("Line: %s\n", line);
            if (tryguess(line, dict[i]) == 1)
            {
                printf("%s: %s\n", dict[i], line);
                
                //i = 0;
                count++;
            }
            //count++;
        }

    }
    printf("%d passwords found.\n",count);
    fclose(fp);
    //free(dict);
}